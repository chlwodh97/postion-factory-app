import 'package:flutter/material.dart';
import 'package:potion_factory_app/components/component_potion_item.dart';
import 'package:potion_factory_app/pages/potionview.dart';

class PageIndex extends StatelessWidget {
  const PageIndex({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(context),
          backgroundColor: Color.fromRGBO(33, 35, 146, 0)
    );
  }

    SliverGridDelegate _sliverGridDelegate() {
      return const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount:3,
          crossAxisSpacing:4,
          mainAxisExtent: 150,
          mainAxisSpacing: 30,
      );
    }


 Widget _buildBody(BuildContext context){
    return Container(
        child: Column(
          children: [
            Container(
              width: 170,
              child: Image.asset('assets/logo.png'),
            ),
            Container(
              width: 170,
              child: Image.asset('assets/200one.png'),
            ),
            Expanded(
              child: GridView(
                gridDelegate: _sliverGridDelegate(),
              children: [
                ComponentPotionItem(
                  imgUrl: 'assets/red_1.png',
                  goodsTitle: '하급 레드포션',
                  goodsPrice: 500,
                  callback: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PotionView()));
                  }
              ),
                ComponentPotionItem(
                    imgUrl: 'assets/red_2.png',
                    goodsTitle: '중급 레드포션',
                    goodsPrice: 1000,
                    callback: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PotionView()));
                    }
                ),
                ComponentPotionItem(
                    imgUrl: 'assets/red_3.png',
                    goodsTitle: '상급 레드포션',
                    goodsPrice: 1500,
                    callback: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PotionView()));
                    }
                ),
                ComponentPotionItem(
                    imgUrl: 'assets/blue_1.png',
                    goodsTitle: '하급 블루포션',
                    goodsPrice: 500,
                    callback: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PotionView()));
                    }
                ),
                ComponentPotionItem(
                    imgUrl: 'assets/blue_2.png',
                    goodsTitle: '중급 블루포션',
                    goodsPrice: 1000,
                    callback: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PotionView()));
                    }
                ),
                ComponentPotionItem(
                    imgUrl: 'assets/blue_3.png',
                    goodsTitle: '상급 블루포션',
                    goodsPrice: 1500,
                    callback: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PotionView()));
                    }
                ),
                ComponentPotionItem(
                    imgUrl: 'assets/yellow.png',
                    goodsTitle: '노란 포션',
                    goodsPrice: 20500,
                    callback: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PotionView()));
                    }
                ),
                ComponentPotionItem(
                    imgUrl: 'assets/green.png',
                    goodsTitle: '초록 포션',
                    goodsPrice: 40000,
                    callback: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PotionView()));
                    }
                ),
                ComponentPotionItem(
                    imgUrl: 'assets/purple.png',
                    goodsTitle: '보라 포션',
                    goodsPrice: 60000,
                    callback: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PotionView()));
                    }
                ),
              ],
              ),
            )

          ],
          ),
    );
 }
}
