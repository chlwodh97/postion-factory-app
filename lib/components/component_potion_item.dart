import 'package:flutter/material.dart';

class ComponentPotionItem extends StatelessWidget {
  const ComponentPotionItem({
    super.key,
    required this.imgUrl,
    required this.goodsTitle,
    required this.goodsPrice,
    required this.callback
  });

  final String imgUrl;
  final String goodsTitle;
  final num goodsPrice;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Image.asset(imgUrl , width: 100, height: 100,),
            Text(goodsTitle),
            Text('${goodsPrice}G')
          ],
        ),
      ),
    );
  }
}

